#!/usr/bin/env python3
###########################################################################
##                                                                       ##
##                  Language Technologies Institute                      ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2011                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  Author: Alok Parlikar (aup@cs.cmu.edu)                               ##
##  Date  : November 2011                                                ##
###########################################################################
"""
Read in a list of wav files from stdin, and write their transcriptions
(via Google Speech Recognition Service) on stdout
"""

from google_asr import decode_wavefile
import sys
import time

if __name__ == '__main__':
    if len(sys.argv) != 1:
        print("Usage: ls -1 wave-dir/*.wav | %s > output", file=sys.stderr)
        sys.exit(1)

    for line in sys.stdin:
        wave_filename = line.strip()
        asr_result = decode_wavefile(wave_filename)
        if not asr_result:
            best_hypothesis = ""
            best_confidence = 0.0
        else:
            best_hypothesis = asr_result[0]['utterance']
            best_confidence = asr_result[0]['confidence']
        print('\t'.join([wave_filename,
                         best_hypothesis,
                         "%1.4f" % best_confidence]))
        # Don't rush queries to google servers
        time.sleep(0.2)
