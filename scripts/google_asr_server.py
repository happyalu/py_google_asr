#!/usr/bin/env python3
###########################################################################
##                                                                       ##
##                  Language Technologies Institute                      ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2011                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  Author: Alok Parlikar (aup@cs.cmu.edu)                               ##
##  Date  : November 2011                                                ##
###########################################################################
"""
Run a TCP server that runs google ASR and acts as a drop-in
replacement for the pocketsphinx decoder in server mode.

This implements the pocketsphinx protocol to receive audio data and
sends it over to Google Speech Recognition Service for transcription.
"""

from google_asr import decode_wavefile
from tempfile import NamedTemporaryFile
from socketserver import StreamRequestHandler, ThreadingMixIn, TCPServer


def _debug(message):
    """Write debug message to console
    Arguments:
    - `message`: Message to Display
    """
    print(message)


class _ThreadedTCPServer(ThreadingMixIn, TCPServer):
    """
    A class that uses multiple inheritance to get the functionality of
    a multi-threaded TCP Server
    """
    # We don't need to add any more functionality to this class
    pass


class _PocketSphinxProtocolRequestHandler(StreamRequestHandler):
    """
    Implements a StreamRequestHandler that understands the
    pocketsphinx protocol for decoding
    """

    def handle(self):
        """
        Handle an incoming TCP request and send an appropriate reply
        """

        # Description of the implemented pocketsphinx protocol:
        #
        # C is client, S is server
        # C: engine_new_session
        # S: <no reply>
        # C: engine_begin_utt
        # S: <no reply>
        # C: engine_proc_raw NumBytes
        # S: <expect to read NumBytes sized data>
        # C: <data>
        # S: <keep reading data>
        # C: engine_end_utt
        # S: <send recorded data to google for ASR
        # C: engine_proc_result
        # S: asr_output

        # Socket details:
        # self.rfile is a file-like object created by StreamRequestHandler;
        # Likewise, self.wfile is a file-like object used to write back

        wav_data = b""
        asr_output = ""

        for line in self.rfile:
            line = line.strip()
            _debug("Received command: %s" % line)

            if line == b'engine_new_session':
                # Nothing special to do for new session
                pass

            if line.startswith(b"engine_begin_utt"):
                # Empty any cached waveform since a new utt is starting
                wav_data = b""

            if line.startswith(b"engine_proc_raw"):
                # Prepare to receive raw audio samples
                cmd, numbytes = line.split()
                numbytes = int(numbytes)

                # Read numbytes from file in a blocked call
                _debug("Reading %d bytes as wave data" % numbytes)
                wav_data += self.rfile.read(numbytes)

            if line == b'engine_end_utt':
                # Decode the wav data and save the output until the
                # engine_proc_result command is called to retrieve it.

                raw_file = NamedTemporaryFile()
                raw_file.write(wav_data)
                raw_file.flush()

                asr_output_list = decode_wavefile(raw_file.name,
                                             wavefile_has_header=False,
                                             unheadered_sample_rate=16000,
                                             unheadered_sample_type='signed',
                                             unheadered_sample_bitsize=16,
                                             max_results=1,
                                             profanity_filter=True)
                try:
                    asr_output = asr_output_list[0]['utterance']
                except (IndexError, KeyError):
                    # ASR failed to run. Send an empty response
                    asr_output = ""
                raw_file.close()

            if line == b'engine_proc_result':
                # Append a newline to the ASR output and send the result
                asr_output = "%s\n" % asr_output

                _debug("Sending hypothesis to decoder: %s" % asr_output)
                self.wfile.write(asr_output.encode('utf-8'))

if __name__ == '__main__':
    # Parse commandline arguments
    import argparse
    parser = argparse.ArgumentParser(
        description='Start PocketSphinx-like server for Google ASR')
    parser.add_argument('--bind-address', '-a', type=str, default='127.0.0.1',
                        help="IP Address to bind to. Defaults to 127.0.0.1")
    parser.add_argument('--bind-port', '-p', type=int, default=9993,
                        help="TCP Port to bind to. Defaults to 9993")
    args = parser.parse_args()

    host = args.bind_address
    port = args.bind_port

    # Start the server
    server = _ThreadedTCPServer((host, port),
                                _PocketSphinxProtocolRequestHandler)
    server.serve_forever()
